public enum BaseCar {
  OPEL("OPEL", 8000),
  AUDI("AUDI", 15000),
  BMW("BMW", 20000),
  MERCEDES("MERCEDES", 25000),
  KIA("KIA", 4000);

  private String carName;
  private int carPrice;

  BaseCar(String carName, int carPrice) {
    this.carName = carName;
    this.carPrice = carPrice;
  }

  public String getCarName() {
    return carName;
  }

  public void setCarName(String carName) {
    this.carName = carName;
  }

  public int getCarPrice() {
    return carPrice;
  }

  public void setCarPrice(int carPrice) {
    this.carPrice = carPrice;
  }
}
