public enum Upholstery {

    VELOUR("VELOUR", 0),
    LEATHER("LEATHER", 1000),
    QUILTED_LEATHER("QUILTED_LEATHER", 2000);

    Upholstery(String upholsteryName, int upholsteryPrice) {
        this.upholsteryName = upholsteryName;
        this.upholsteryPrice = upholsteryPrice;
    }

    private String upholsteryName;
    private int upholsteryPrice;

    public String getUpholsteryName() {
        return upholsteryName;
    }

    public void setUpholsteryName(String upholsteryName) {
        this.upholsteryName = upholsteryName;
    }

    public int getUpholsteryPrice() {
        return upholsteryPrice;
    }

    public void setUpholsteryPrice(int upholsteryPrice) {
        this.upholsteryPrice = upholsteryPrice;
    }
}
