public class Customer {

  double budget;

  public Customer(double budget) {
    this.budget = budget;
  }

  public double getBudget() {
    return budget;
  }

  public void setBudget(double budget) {
    this.budget = budget;
  }


}
