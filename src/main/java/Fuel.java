public enum Fuel {
    GASOLINE("GASOLINE", 0),
    DIESEL("DIESEL", 10000),
    HYBRID("HYBRIR", 15000);


    private String fuelName;
    private int fuelPrice;
    Fuel(String fuelName, int fuelPrice) {
        this.fuelName = fuelName;
        this.fuelPrice = fuelPrice;
    }

    public String getFuelName() {
        return fuelName;
    }

    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    public int getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(int fuelPrice) {
        this.fuelPrice = fuelPrice;
    }
}