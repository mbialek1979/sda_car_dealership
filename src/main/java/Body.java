public enum Body {

  SEDAN("SEDAN", 0),
  PICKUP("PICKUP", 2000),
  HATCHBACK("HATCHBACK", 1000),
  ESTATECAR("ESTATE_CAR", 1000);

  private String bodyName;
  private int bodyPrice;
  Body(String bodyName, int bodyPrice) {
    this.bodyName = bodyName;
    this.bodyPrice = bodyPrice;
  }

  public String getBodyName() {
    return bodyName;
  }

  public void setBodyName(String bodyName) {
    this.bodyName = bodyName;
  }

  public int getBodyPrice() {
    return bodyPrice;
  }

  public void setBodyPrice(int bodyPrice) {
    this.bodyPrice = bodyPrice;
  }
}
