public class FinalCar {

  BaseCar baseCar;
  Body body = Body.SEDAN;
  Fuel fuel = Fuel.GASOLINE;
  Colour colour = Colour.WHITE;
  Upholstery upholstery = Upholstery.VELOUR;
  double price;

  public FinalCar(BaseCar baseCar, Body body, Fuel fuel, Colour colour, Upholstery upholstery) {
    this.baseCar = baseCar;
    this.body = body;
    this.fuel = fuel;
    this.colour = colour;
    this.upholstery = upholstery;
    this.price = baseCar.getCarPrice() + body.getBodyPrice() + colour.getColourPrice() + upholstery
        .getUpholsteryPrice();
  }

  public BaseCar getBaseCar() {
    return baseCar;
  }

  public void setBaseCar(BaseCar baseCar) {
    this.baseCar = baseCar;
  }

  public Body getBody() {
    return body;
  }

  public void setBody(Body body) {
    this.body = body;
  }

  public Fuel getFuel() {
    return fuel;
  }

  public void setFuel(Fuel fuel) {
    this.fuel = fuel;
  }

  public Colour getColour() {
    return colour;
  }

  public void setColour(Colour colour) {
    this.colour = colour;
  }

  public Upholstery getUpholstery() {
    return upholstery;
  }

  public void setUpholstery(Upholstery upholstery) {
    this.upholstery = upholstery;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  @Override
  public String toString() {

    String carDescribe =
        "Your choice is: " + this.baseCar.getCarName() + ":\n" + "Total price is: " + this
            .getPrice() + "\n" + this.body.getBodyName() + ", " +
            this.fuel.getFuelName() + ", " + this.colour.getColourName() + ", " + this.upholstery
            .getUpholsteryName();
    return carDescribe;
  }
}
