public enum Colour {

    WHITE ("WHITE", 0),
    BLACK("BLACK", 1000),
    RED("RED", 1000),
    YELLOW("YELLOW", 1000)
    ;


    Colour(String colourName, int colourPrice) {
        this.colourName = colourName;
        this.colourPrice = colourPrice;
    }

    private String colourName;
    private int colourPrice;

    public String getColourName() {
        return colourName;
    }

    public void setColourName(String colourName) {
        this.colourName = colourName;
    }

    public int getColourPrice() {
        return colourPrice;
    }

    public void setColourPrice(int colourPrice) {
        this.colourPrice = colourPrice;
    }
}
